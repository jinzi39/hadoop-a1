from pyspark import SparkConf, SparkContext
from operator import add
import sys
import csv

def compare(item1, item2):
	i1 = item1.split(",")
	i2 = item2.split(",")
	if i1[2] < i2[2]:
		return -1
	elif i1[2] > i2[2]:
		return 1
	else:
		return i1[-1] > i2[-1]

def write_list_to_file(guest_list, filename):
	with open(filename, "w") as outfile:
	    for entries in guest_list:
	        outfile.write(entries)
	        outfile.write("\n")

if __name__ == '__main__':

	if len(sys.argv) != 3:
		print 'Incorrect Number of Arguments'
		exit(-1) 

	fileName = sys.argv[1]
	output = sys.argv[2]

	# Set spark configurations
	conf = (SparkConf()
			.setAppName('CS-744-Assignment1-Part2').set("spark.locality.wait", 0))

	# Initialize the spark context.
	sc = SparkContext(conf = conf)

	# Load the input file 
	lines = sc.textFile(fileName)

	res = lines.collect()

	res[1:] = sorted(res[1:], cmp=compare)

	write_list_to_file(res, output)

	# print res
	
	sc.stop()