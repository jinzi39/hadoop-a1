#! /usr/bin/env bash

#
# hadoop setup, to be called from setup-server.sh
#

echo "Getting and setting up hadoop."
$PSSHCLI wget -c -nv http://apache.mirrors.hoobly.com/hadoop/common/hadoop-2.7.6/hadoop-2.7.6.tar.gz
$PSSHCLI rm -rf hadoop-2.7.6/
$PSSHCLI tar zxf hadoop-2.7.6.tar.gz
echo "Getting and setting up hadoop... Done."


echo "Update hadoop configuration."
$PSSHCLI 'sed -i "/<configuration>/a \
<property>\n\
    <name>fs.default.name</name>\n\
    <value>hdfs://"'$CLI1IP'":9000</value>\n\
</property>" hadoop-2.7.6/etc/hadoop/core-site.xml'
echo "Update hadoop configuration... Done."


echo "Add data folders..."
NAMEDIR=~/hadoop/data/namenode
DATADIR=~/hadoop/data/datanode

$PSSHCLI mkdir -p $NAMEDIR
$PSSHCLI mkdir -p $DATADIR

$PSSHCLI 'sed -i "/<configuration>/a \
<property>\n\
    <name>dfs.namenode.name.dir</name>\n\
    <value>"'$NAMEDIR'"</value>\n\
</property>\n\
<property>\n\
    <name>dfs.datanode.data.dir</name>\n\
    <value>"'$DATADIR'"</value>\n\
</property>" hadoop-2.7.6/etc/hadoop/hdfs-site.xml'
echo "Add data folders... Done."


echo "Fixing JAVA_HOME"
JREPATH=`update-alternatives --display java | awk '/currently/ { print $5 }'`
JREPATH=${JREPATH%/bin/java}
if [ "$JREPATH" == "" ]
then
    # because, hey, this fails sometimes
    JREPATH="/usr/lib/jvm/java-8-openjdk-amd64/jre"
fi
export JREPATH
$PSSHCLI 'sed -i "s,JAVA_HOME=.*,JAVA_HOME="'${JREPATH}'"," hadoop-2.7.6/etc/hadoop/hadoop-env.sh'
echo "Fixing JAVA_HOME... Done."


echo "Copying slavefile..."
SLAVESFILE="hadoop-2.7.6/etc/hadoop/slaves"
scp slaves $CLI1:~/$SLAVESFILE
scp slaves $CLI2:~/$SLAVESFILE
scp slaves $CLI3:~/$SLAVESFILE
echo "Copying slavefile... Done."


echo "Setting PATH..."
$PSSHCLI 'echo "PATH=~/hadoop-2.7.6/sbin:~/hadoop-2.7.6/bin:\$PATH" >> .profile'
echo "Setting PATH... Done."


# use "bash --login -c" to use updated path in ssh environment.
# use it a second time to use the updated path in a sudo environment.
echo "Starting Hadoop..."
ssh $CLI1 'bash --login -c "hdfs namenode -format"'
ssh $CLI1 'bash --login -c "stop-dfs.sh"'
ssh $CLI1 'bash --login -c "start-dfs.sh"'
echo "Starting Hadoop... Done."
