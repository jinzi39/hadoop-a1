#! /usr/bin/env bash

echo "Loading HDFS Data..."
hdfs dfs -mkdir -p /test-data/enwiki-pages-articles
for i in `seq 1 10`
do
    hdfs dfs -put /test-data/enwiki-pages-articles/link-enwiki-20180601-pages-articles$i.xml* /test-data/enwiki-pages-articles/
done
echo "Loading HDFS Data... Done."

pushd spark-2.2.0-bin-hadoop2.7

echo "Editing example pagerank..."
sed -i -e '/.appName/a \
        .config("spark.default.parallelism", "1")\
        .config("spark.driver.memory", "8g")\
        .config("spark.executor.memory", "8g")\
        .config("spark.executor.cores", "5")\
        .config("spark.task.cpus", "1")' \
    -e 's/ranks\.collect()/ranks\.takeOrdered(100, key = lambda x: -x[1])/' \
    -e 's/\(.map(lambda r: r\[0\])\)/\1.filter(lambda line: (not (":" in line)) or line.startswith("Category:"))/' \
    examples/src/main/python/pagerank.py
echo "Editing example pagerank... Done."


# run example pagerank
echo "Running example pagerank..."
spark-submit \
    examples/src/main/python/pagerank.py \
    hdfs://`hostname -f`:9000/test-data/enwiki-pages-articles/link-enwiki-20180601-pages-articles1.xml-p10p30302 \
    1
echo "Running example pagerank... Done."

popd
