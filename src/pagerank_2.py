from pyspark import SparkConf, SparkContext

ITERATIONS = 10

def get_contributions(contributions):
        rank = (contributions[1][1])
        neighbors = contributions[1][0]
        num_neighbors = len(neighbors)
        contribution = float(rank)/num_neighbors
        for neighbor in neighbors:
                yield (neighbor, contribution)

if __name__ == '__main__':

        fileName = "sample.txt"
        #fileName = "web-BerkStan.txt"

        # Set spark configurations
        conf = (SparkConf()
                .setAppName('CS-744-Assignment1-Part2').set("spark.locality.wait", 0))
        
        # Initialize the spark context.
        sc = SparkContext(conf = conf)

        # Load the input file
        lines = sc.textFile(fileName)

        # filer # lines
        lines = lines.filter(lambda l: "#" not in l)

        # initialize links
        links = lines.map(lambda x: x.split("\t")).groupByKey()

        # initialize ranks
        ranks = links.keys().map(lambda x: (x,1))

        # Run PageRank
        for i in range(ITERATIONS):
                #print(i)
                # sum contributions
                contributions = links.join(ranks).flatMap(get_contributions)
                #print(contributions.take(5))
                # update ranks
                ranks = contributions.reduceByKey(lambda x, y: x + y).mapValues(lambda contribution: 0.15 + 0.85*contribution)
                #print(ranks.take(5))

        #stop Spark
        sc.stop()
