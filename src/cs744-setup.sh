#! /usr/bin/env bash

tryTryAgain () {
    false  # set up $?

    while [ $? = 1 ]
    do
        `$@`

	if [ $? = 1 ]
	then
	    sleep 2
	fi
    done
}

# take hostname as script parameter.
if [ ! -z $1 ]
then

    HOSTNAME=$1; export HOSTNAME
    HOSTNAME="${HOSTNAME%%.*}"
else
    echo "Need user and primary server (not VM server)."
    echo "Usage:"
    echo "    $0 user@servername"
    exit
fi

#
# running on local machine.
#
CLOUDLAB="wisc.cloudlab.us"; export CLOUDLAB
NODE0="${HOSTNAME}.${CLOUDLAB}"; export NODE0
CLI1="${HOSTNAME}vm-1.${CLOUDLAB}"; export CLI1
CLI2="${HOSTNAME}vm-2.${CLOUDLAB}"; export CLI2
CLI3="${HOSTNAME}vm-3.${CLOUDLAB}"; export CLI3

IDFILE=id_rsa; export IDFILE
if [ ! -f ./$IDFILE ]
then
    echo "You don't have a project-specific SSH key, creating one."
    ssh-genkey -t rsa -f ./$IDFILE
fi

# since we can't continue until the ssh keys are installed, repeat
# until they are, ignoring transient name resolution failures.
tryTryAgain scp -o StrictHostKeyChecking=no ./$IDFILE* $NODE0:~/.ssh/
echo "installed key on p-node."
tryTryAgain scp -o StrictHostKeyChecking=no ./$IDFILE* $CLI1:~/.ssh/
echo "installed key on vm-1."
tryTryAgain ssh-copy-id -i ./$IDFILE.pub -o StrictHostKeyChecking=no $NODE0
echo "copied pubkey to p-node."
tryTryAgain ssh-copy-id -i ./$IDFILE.pub -o StrictHostKeyChecking=no $CLI1
echo "copied pubkey to vm-1."
tryTryAgain ssh-copy-id -i ./$IDFILE.pub -o StrictHostKeyChecking=no $CLI2
echo "copied pubkey to vm-2."
tryTryAgain ssh-copy-id -i ./$IDFILE.pub -o StrictHostKeyChecking=no $CLI3
echo "copied pubkey to vm-3."

for i in `seq 1 5`;
do
    # no, really, this needs to work, let's try a few times to make sure.
    echo "Host registration pass $i of 5..."

    echo "  Registering p-node => vms as trusted..."
    tryTryAgain ssh -o StrictHostKeyChecking=no $CLI1 exit &
    tryTryAgain ssh -o StrictHostKeyChecking=no $CLI2 exit &
    tryTryAgain ssh -o StrictHostKeyChecking=no $CLI3 exit &
    echo "  Registering p-node => vms as trusted... Done."

    echo "Host registration pass $i of 5... Done."
done

for i in `seq 1 5`;
do
    echo "VM-1 registration pass $i of 5..."

    echo "  Registering vm1 => vms as trusted..."
    tryTryAgain ssh $CLI1 "ssh -o StrictHostKeyChecking=no $CLI1 'exit'" &
    tryTryAgain ssh $CLI1 "ssh -o StrictHostKeyChecking=no $CLI2 'exit'" &
    tryTryAgain ssh $CLI1 "ssh -o StrictHostKeyChecking=no $CLI3 'exit'" &
    tryTryAgain ssh $CLI1 "ssh -o StrictHostKeyChecking=no 0.0.0.0 'exit'" &
    echo "  Registering vm1 => vms as trusted... Done."

    echo "VM-1 registration pass $i of 5... Done."
done

#
# running on primary node
#
ssh $NODE0 rm -rf hadoop-a1
ssh $NODE0 git clone https://gitlab.com/nickdaly/hadoop-a1
ssh $NODE0 hadoop-a1/src/setup-server.sh
