#! /usr/bin/env bash

#
# spark setup, to be called from setup-server.sh
#

$PSSHCLI wget -c -nv https://d3kbcqa49mib13.cloudfront.net/spark-2.2.0-bin-hadoop2.7.tgz
echo "spark downloaded"

$PSSHCLI rm -rf spark-2.2.0-bin-hadoop2.7/
$PSSHCLI tar zxf spark-2.2.0-bin-hadoop2.7.tgz
echo "spark unzipped"

SLAVESFILE="spark-2.2.0-bin-hadoop2.7/conf/slaves"
scp slaves $CLI1:~/$SLAVESFILE
scp slaves $CLI2:~/$SLAVESFILE
scp slaves $CLI3:~/$SLAVESFILE
echo "slaves added"

$PSSHCLI 'echo "PATH=~/spark-2.2.0-bin-hadoop2.7/sbin:~/spark-2.2.0-bin-hadoop2.7/bin:\$PATH" >> .profile'
echo "path modded"

# use "bash --login -c" to use updated path.
ssh $CLI1 'bash --login -c stop-all.sh'
ssh $CLI1 'bash --login -c start-all.sh'
echo "Spark started"
