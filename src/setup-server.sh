#! /usr/bin/env bash

HOSTNAME=`hostname -f`
HOSTNAME="`whoami`@${HOSTNAME%%.*}"; export HOSTNAME
CLOUDLAB="wisc.cloudlab.us"; export CLOUDLAB
NODE0="${HOSTNAME}.${CLOUDLAB}"; export NODE0
PSSHSVR="parallel-ssh -i -O StrictHostKeyChecking=no -h servers"; export PSSHSVR
PSSHCLI="parallel-ssh -i -O StrictHostKeyChecking=no -h clients"; export PSSHCLI
CLI1="${HOSTNAME}vm-1.${CLOUDLAB}"; export CLI1
CLI2="${HOSTNAME}vm-2.${CLOUDLAB}"; export CLI2
CLI3="${HOSTNAME}vm-3.${CLOUDLAB}"; export CLI3
CLI1IP=`host "${CLI1##*@}" | cut -d" " -f 4`; export CLI1IP
CLI2IP=`host "${CLI2##*@}" | cut -d" " -f 4`; export CLI2IP
CLI3IP=`host "${CLI3##*@}" | cut -d" " -f 4`; export CLI3IP

# all servers
echo "$NODE0
$CLI1
$CLI2
$CLI3" > servers
# only virtual clients' hostnames
echo "$CLI1
$CLI2
$CLI3" > clients
# only virtual clients' ips
echo "$CLI1IP
$CLI2IP
$CLI3IP" > slaves

# install packages
sudo apt-get -y update --fix-missing
sudo apt-get -y install pssh
$PSSHSVR sudo apt-get -y update --fix-missing
$PSSHSVR sudo apt-get -y install openjdk-8-jdk git pssh
$PSSHSVR rm -rf ~/hadoop-a1
$PSSHSVR git clone https://gitlab.com/nickdaly/hadoop-a1

#
# hadoop!
#
hadoop-a1/src/setup-hadoop.sh

#
# spark
#
hadoop-a1/src/setup-spark.sh
